/**
 * Stuffing or unstuffing information for transmission
 */
public class BitStuffing {
    private static String flags = "01111110";

    /**
     * Stuffing a trame to send with flags at beginning and at end, also add '0' after five '1' detected
     * @param trame Trame to stuff
     * @return  Trame Stuffed
     */
    public static String stuffing(String trame){
        String stuffedTrame = flags;
        int index = 0;
        int nbOne = 0;

        while (index < trame.length()){
            String bin = trame.substring(index, index+1);

            if (bin.equals("1"))
                nbOne++;
            else
                nbOne = 0;

            stuffedTrame += bin;

            if (nbOne == 5)
                stuffedTrame += "0";
            index++;
        }
        return stuffedTrame  + flags;
    }


    /**
     * Unstuffing a trame received by removing flags at beginning and end, also removing '0' after five '1' detected
     * @param stuffedTrame Trame Stuffed
     * @return Trame Unstuffed
     */
    public static String unstuffing(String stuffedTrame){
        //check flags
        if (!stuffedTrame.substring(0,8).equals(flags) || !stuffedTrame.substring(stuffedTrame.length()-8).equals(flags))
            return null;

        String cleanTrame = "";
        int index = 8;
        int nbOne = 0;

        while (index < stuffedTrame.length()- 8){
            String bin = stuffedTrame.substring(index, index + 1);

            if (bin.equals("1"))
                nbOne++;
            else
                nbOne = 0;

            cleanTrame += bin;

            if (nbOne == 5)
                index++;
            index++;
        }
        return cleanTrame;
    }



}
