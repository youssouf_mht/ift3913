/**
 * Usefull class to convert something
 */
public class Converter {
    /**
     * Converting a String to Binary information
     * @param input A String to convert
     * @return  A binary Chain
     */
    public static String stringToBinary(String input) {
        StringBuilder result = new StringBuilder();
        char[] chars = input.toCharArray();

        for (char aChar : chars)
            result.append(String.format("%8s", Integer.toBinaryString(aChar)).replaceAll(" ", "0"));
        return result.toString();
    }

    /**
     * Converting a Binary information to String
     * @param binary A binary chain to convert
     * @return  Original String
     */
    public static String binaryToString(String binary){
        int index = 0;
        String msg = "";

        while (index < binary.length()){
            int charCode = Integer.parseInt(binary.substring(index, Math.min(index+8, binary.length())), 2);
            msg += Character.toString((char)charCode);
            index += 8;
        }
        return msg;
    }

    /**
     * Converting a Binary information to Int
     * @param binary Binary Chain
     * @return  A number
     */
    public static int binaryToInt(String binary){
        String s = Converter.binaryToString(binary);
        return Integer.parseInt(s);
    }
}
