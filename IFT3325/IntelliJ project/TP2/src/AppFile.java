import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Read, write, create and transform a file.txt from binary or string
 */
public class AppFile {
    /**
     * Create a txt file from binary information
     * @param binData Binary Chain
     */
    public static void createTxtFromBin(String binData){
        String data = Converter.binaryToString(binData);
        try {
            PrintWriter output = new PrintWriter("Received.txt");
            output.println(data);
            output.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     *Read a file txt and transform it to binary
     * @param filePath The path for file
     * @return Binary Chain in String format
     */
    public static String getFileToBinary(String filePath){
        Scanner openedFile = AppFile.fileCheck(filePath);
        if (openedFile == null)
            return "";

        String binFile = "";
        while (openedFile.hasNextLine())
            binFile += Converter.stringToBinary(openedFile.nextLine() + "\n");
        return binFile;
    }

    /**
     *Check path exist and it's a file
     * @param filePath  The path for file
     * @return Opened file (Scanner)
     */
    private static Scanner fileCheck(String filePath){
        Path path = Paths.get(filePath);
        Boolean isPathExist = Files.exists(path);
        if (!isPathExist) {
            System.out.println("The file doesn't exist.");
            return null;
        }

        Scanner openedFile = null;
        try {
            File file = new File(filePath);
            Boolean isFile = file.isFile();
            if (!isFile) {
                System.out.println("This is not a file.");
                return null;
            }

            openedFile = new Scanner(file);
        } catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
        return openedFile;
    }




}
