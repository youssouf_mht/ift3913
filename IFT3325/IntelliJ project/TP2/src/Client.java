import java.net.*;
import java.io.*;

/**
 * Reference a la commande Sender
 * Creating a Client
 */
public class Client {
    private Socket socket = null;
    private BufferedReader in = null;
    private PrintStream out = null;

    /**
     * Connecting to a opened socket somewhere
     * @param address   Address to connect
     * @param port      Port to connect
     * @param binData   File to send in Binary of String
     * @param networkType   Connection required (Go-Back-N in this case)
     */
    public void start(String address, int port, String binData, int networkType) {
        Transport network = new Transport(binData, networkType);
        try {
            //Connection
            socket = new Socket(address, port);
            System.out.println("Connected");

            //Read and write to the socket
            out = new PrintStream(socket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            //TimeOut reading
            socket.setSoTimeout(100);

            //Transfert Trames process
            while (network.isConnectionClose()) {
                String trame = network.getNextTrame();

                if (trame == null) { //No trame to send
                    try {
                        trame = in.readLine(); //Listen socket
                        trame = BitStuffing.unstuffing(trame);
                        network.verifyTrame(trame);

                        //DISPLAY transmission
                        Display.printL1("\n\n<--- Receiving\t");
                        if (trame == null)
                            Display.printL1("Incorrect Stuffing \t");
                        else
                            Display.showTrame(trame);
                        Display.printL3("\tWindowsSliding " + network.getStart() + " - " + network.getEnd());
                        Display.printL2("");

                    } catch (SocketTimeoutException timeout) {//Nothing to listen
                    }
                } else {

                    //DISPLAY transmission
                    Display.printL1("---> Sending \t");
                    Display.showTrame(trame);
                    Display.printL3("\tWindowsSliding " + network.getStart() + " - " + network.getEnd());
                    trame = CorruptedClient.cTrame(trame);

                    trame = BitStuffing.stuffing(trame);

                    //DISPLAY transmission
                    trame = CorruptedClient.cStuffing(trame);
                    Display.printL2("");

                    if (!CorruptedClient.noSendingCorruption()) {
                        out.println(trame);
                        out.flush();
                    } else
                        Display.printL2("NEVER reach destination ... ");
                }
            }

            Display.printL7("Closing Client");
            socket.close();
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}