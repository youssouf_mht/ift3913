/**
 * Calculate or validate a CRC information
 */
public class CRC {
    private static String CRC = "10001000000100001"; //Polynomial divisor
    private static String CRCvalidation = "0000000000000000";

    /**
     * Add the CRC space and calculate CRC result
     * @param trame A trame without any CRC space
     * @return  A trame with CRC space and CRC value in
     */
    public static String calculCRC(String trame){
        return modulusCRC(trame+CRCvalidation);
    }

    /**
     * Specific CRC calculation
     * @param trame A trame with CRC space with all 0
     * @return  A trame with CRC space with value or CRC
     */
    private static String modulusCRC(String trame){
        int iTrame = 0, iCRC = 0;
        boolean continu = true;

        while (continu){
            int compare = Character.compare(trame.charAt(iTrame), '0');

            //Find the first '1' in trame that is not in CRC control length
            while (compare == 0 && iTrame + CRC.length() < trame.length()){
                iTrame++;
                compare = Character.compare(trame.charAt(iTrame), '0');
            }


            if ((iTrame + CRC.length() < trame.length())){

                //Running division
                while (iCRC < CRC.length()) {
                    compare = Character.compare(trame.charAt(iTrame), CRC.charAt(iCRC));
                    if (compare == 0)
                        trame = trame.substring(0, iTrame) + "0" + trame.substring(iTrame+1);
                    else
                        trame = trame.substring(0, iTrame) + "1" + trame.substring(iTrame+1);
                    iTrame++;
                    iCRC++;
                }
            } else
                continu = false; //No '1' left out of CRC control length

            iTrame = 0;
            iCRC = 0;
        }
        return trame.substring(trame.length()-16);
    }

    /**
     * Verify CRC
     * @param trame A trame with CRC space with value
     * @return CRC is correct?
     */
    public static boolean validationCRC(String trame) {
        //Unsuitable trame
        if (trame == null || trame.length() < 17)
            return false;

        String crc = modulusCRC(trame);
        if (crc.equals(CRCvalidation))
            return true;
        return false;
    }
}
