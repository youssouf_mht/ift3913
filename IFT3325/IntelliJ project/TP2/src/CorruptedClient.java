/**
 * Fake class to corrupt data send by client
 */
public class CorruptedClient {
    private static int indexSend = 0;
    private static  int corruptedStuffing = 1000;
    private static int corruptedTrame = 1000;
    private static int corruptedSending = 7;

    public static void setCorruptedStuffing(int corruptedStuffing) {
        CorruptedClient.corruptedStuffing = corruptedStuffing;
    }

    public static void setCorruptedTrame(int corruptedTrame) {
        CorruptedClient.corruptedTrame = corruptedTrame;
    }

    public static void setCorruptedSending(int corruptedSending) {
        CorruptedClient.corruptedSending = corruptedSending;
    }

    /**
     * Change information in original trame (After CRC)
     * @param trame Trame to corrupt
     * @return  Corrupted trame
     */
    public static String cTrame(String trame){
        indexSend = (indexSend + 1)%10;

        if (indexSend == corruptedTrame){
            char c = trame.charAt(20);
            if (c == '1')
                c = '0';
            else
                c = '1';

            Display.printL1(" CORRUPTED ");
            return trame.substring(0,20) + c + trame.substring(21);
        }
        return trame;
    }

    /**
     * Changement dans le stuffing
     * @param trame Trame Stuffed
     * @return  Trame with change in Stuffing
     */
    public static String cStuffing(String trame){
        if (indexSend == corruptedStuffing){
            Display.printL1(" CORRUPTED ");
            return trame.substring(0,3) + '0' + trame.substring(4);
        }
        return trame;
    }

    /**
     * Block the sending (Loss Trame)
     * @return
     */
    public static boolean noSendingCorruption(){
        if (corruptedSending == indexSend)
            return true;
        return false;
    }
}
