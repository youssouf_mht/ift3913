import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Special class to Display hidden process information
 */
public class Display {
    private static int LEVEL = 10;

    /**
     * Show binary information for easily reading
     * @param binary    Binary Chain
     * @param blockSize Spacing between binary block
     * @param separator Which spacing to use
     * @return  A beautiful Binary Sting easily readable
     */
    public static String prettyBinary(String binary, int blockSize, String separator) {
        List<String> result = new ArrayList<>();
        int index = 0;

        while (index < binary.length()) {
            result.add(binary.substring(index, Math.min(index + blockSize, binary.length())));
            index += blockSize;
        }
        return result.stream().collect(Collectors.joining(separator));
    }

    public static void printL0(String info){
        System.out.println(info);
    }
    public static void printL1(String info){
        if (LEVEL > 1) System.out.print(info);
    }
    public static void printL2(String info){
        if (LEVEL > 2) System.out.println(info);
    }
    public static void printL3(String info){
        if (LEVEL > 3) System.out.print(info);
    }
    public static void printL7(String info){
        if (LEVEL > 7) System.out.println(info);
    }

    /**
     * Showing Trame information for easily reading
     * @param trame A trame
     */
    public static void showTrame(String trame) {
        System.out.print("|-- " +
                Converter.binaryToString(trame.substring(0,8)) + " " +
                        Converter.binaryToString(trame.substring(8,16)) + " " +
                                "Data " + Converter.binaryToString(trame.substring(16, trame.length()-16)).length() + " octets --|");
    }
}
