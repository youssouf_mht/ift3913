import java.util.ArrayList;

/**
 * Data are segmented in a maximum length and send
 * Or stacking data received
 */
public class Transport {
    private int MAX_DATA = 80;
    private int MAX_TRAME = 8;
    private ArrayList<String> segData = new ArrayList<>();
    private GoBackN protocol;

    public Transport(String binData, int networkType){
        buildSegmentedData(binData);
        this.protocol = new GoBackN(this, MAX_TRAME, true);
    }

    public Transport(){
        this.protocol = new GoBackN(this, MAX_TRAME, false);
    }

    public int getStart(){
        return this.protocol.getStart();
    }

    public int getEnd(){
        return this.protocol.getEnd();
    }

    public boolean haveData(){
        if (segData.size() > 0)
            return true;
        return false;
    }

    public String getData(){
        String data = "";
        for (String s: segData) {
            data += s;
        }
        return data;
    }

    public void setNextData(String binData){
        segData.add(binData);
    }

    public String getNextData(){
        String data;
        if (segData.size() != 0){
            data = segData.get(0);
            segData.remove(0);
        } else
            data = null;
        return data;
    }


    /**
     * Send a Trame required
     * @return A trame
     */
    public String getNextTrame(){
        String trame = protocol.getNextTrame();
        return trame;
    }

    public boolean isConnectionClose(){
        return protocol.isConnectionClose();
    }

    /**
     * Verify a trame received
     * @param trame A trame
     */
    public void verifyTrame(String trame){
        protocol.verifyTrame(trame);
    }

    /**
     * Segmentation of data in maximum length required
     * @param binData A file in baniry format
     */
    private void buildSegmentedData(String binData){
        while (true){
            segData.add(binData.substring(0,Math.min(MAX_DATA, binData.length())));
            if (MAX_DATA >= binData.length())
                return;
            binData = binData.substring(MAX_DATA);
        }
    }
}
