/**
 * Transmission protocol between Clients
 */
public class GoBackN {
    private int start = 0, end = -1; //Sliding window
    private String[] trames;
    private Transport trans;
    private long time = System.currentTimeMillis();
    private boolean isSending;
    private boolean isConnected = true;
    private boolean isReady;
    private boolean isCompleted = false;
    private boolean haveToResend = false;

    public GoBackN(Transport transport, int maxTrame, boolean isSending){
        this.trans = transport;
        this.trames = new String[maxTrame];
        this.isSending = isSending;
        this.isReady = isSending;

        for (String s: trames) {
            s = null;
        }
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    private void setTime(){this.time = System.currentTimeMillis();}

    /**
     * Send the next Trame for either receiver or Sender
     * @return A trame
     */
    public String getNextTrame() {
        if (isSending) {
            String trame = sendNextTrame();
            return trame;
        }
        else {
            String trame = sendLastVerifyTrame();
            return trame;
        }
    }

    /**
     *  Get next trame to send from Sender
     * @return A trame
     */
    private String sendNextTrame(){
        //Time > 3 sec or received a corrupted trame
        if (this.haveToResend || (System.currentTimeMillis() - this.time)/1000F > 3) {
            setTime();
            this.haveToResend = false;
            return Trame.buildTrame("P",0,"");
        }

        //Send first trame (Communication Confirmation)
        if (end == -1) {
            //Send 1st trame every second if there is no Acknowledge received
            if ((System.currentTimeMillis()-this.time)/1000F > 1) {
                setTime();
                return Trame.buildTrame("C", 0, "");
            }
           return null;
        }

        //Last trame have been send
        if (this.isCompleted) {
            int temp = (end + 1)%this.trames.length;
            //Confirm all trame received
            if (start == temp)
                //Send communication Close
                if (this.isReady) {
                    setTime();
                    this.isReady = false;
                    return Trame.buildTrame("F", 0, "");
                } else
                    return null;
            else
                return null;
        }

        //Max slidingWindow = no more trame can be send
        if ((end+2)%trames.length == start)
            return null;

        //Special case : Resending last data after Rejection
        if (!trans.haveData() && this.trames[(end + 1)%this.trames.length] == null){
            this.isCompleted = true;
            return null;
        }
        //increase window end
        end = (end + 1)%this.trames.length;

        if (this.trames[end] == null){ //Have to create next trame
            String data = this.trans.getNextData();
            this.isCompleted = !trans.haveData();
            setTime();
            this.trames[end] = Trame.buildTrame("I", end, data);
            return this.trames[end];
        } else { //There is some Trame to resend (After a rejection)
            setTime();
            return this.trames[end];
        }
    }

    /**
     *  Receiver : Send last confirmed Trame
     * @return A trame
     */
    private String sendLastVerifyTrame(){
        if (isReady) { //Prevent multiple confirmation sending for the receiver
            this.isReady = false;
            int temp = (end + 1)%this.trames.length;
            return this.trames[temp];
        } else
            return null;
    }

    /**
     * Connection is open?
     * @return
     */
    public boolean isConnectionClose(){
        return this.isConnected;
    }

    /**
     * Verify an incoming Trame
     * @param trame A trame
     */
    public void verifyTrame(String trame){
        setTime();
        if (CRC.validationCRC(trame)){
            char type = Trame.getType(trame);
            int nbTrame = Trame.getNumber(trame);
            int temp;

            switch (type){
                //Receiver Section
                case 'I':
                    if (nbTrame == (end+1)%this.trames.length){ //Confirm Trame is really the one we wait for
                        //Adding Trame
                        end = (end + 1)%this.trames.length;
                        this.trames[end] = trame;

                        //Save Data
                        trans.setNextData(Trame.getData(trame));

                        //Create Confirm trame
                        temp = (end + 1)%this.trames.length;
                        this.trames[temp] = Trame.buildTrame("A", temp, "");
                    } else {
                        //Trame rejected
                        temp = (end + 1)%this.trames.length;
                        this.trames[temp] = Trame.buildTrame("R", temp, "");
                    }
                    start = end;
                    this.isReady = true;
                    break;

                case 'C':
                    //save Trame
                    end = (end + 1)%this.trames.length;
                    this.trames[end] = trame;
                    this.isReady = true;

                    //Create confirmation Trame
                    temp = (end + 1)%this.trames.length;
                    this.trames[temp] = Trame.buildTrame("A", temp, "");
                    break;

                case 'P':
                    this.isReady = true;
                    break;


                    // Sender Section
                case 'A':
                    if (end == -1) //Special case for Openning connection Acknowledge
                        end = (end + 1)%this.trames.length;
                    else {
                        //Reset window
                        while (start != nbTrame){
                            this.trames[start] = null;
                            start = (start + 1)%this.trames.length;
                        }
                        start = nbTrame;
                    }
                    break;

                case 'R':
                    //Special case for Opening Connection
                    if (end == -1)
                        return;

                    //Reset Window
                    if (nbTrame == 0)
                        end = trames.length-1;
                    else
                        end = nbTrame - 1;

                    //Reset Window
                    while (start != nbTrame){
                        this.trames[start] = null;
                        start = (start + 1)%this.trames.length;
                    }
                    start = nbTrame;
                    this.isReady = true;
                    this.isCompleted = false;
                    break;

                    //Both
                case 'F':
                    //Closing Connection Acknowledge
                    this.isReady = true;
                    this.isConnected = false;
                    temp = (end + 1)%this.trames.length;
                    this.trames[temp] = Trame.buildTrame("F", 0, "");
                    break;

            }
        } else {
            if (!isSending) {
                //Make a Rejection Trame
                this.isReady = true;
                int temp = (end + 1)%this.trames.length;
                this.trames[temp] = Trame.buildTrame("R", temp, "");
            } else {
                //Just resend last trame
                this.haveToResend = true;
            }
        }
    }
}
