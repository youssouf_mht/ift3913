import java.net.*;
import java.io.*;

/**
 * Refer to command Receiver
 * Creating a Server
 */
public class Server {
    private Socket socket = null;
    private ServerSocket server = null;
    private BufferedReader in = null;
    private PrintStream out = null;

    /**
     * Create a server, receive data, return data received once closing communication
     * @param port Port to open
     * @return  The complete information received in Socket
     */
    public String getBinFile(int port) {

        try {
            //Open receiving port
            server = new ServerSocket(port);
            Display.printL0("Server started");
            Display.printL0("Waiting for a client ...");

            //Wait for a client
            socket = server.accept();
            Display.printL0("Client accepted");

            //Read and write to the socket
            out = new PrintStream(socket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            //TimeOut Reading
            socket.setSoTimeout(200);

            Transport network = new Transport();

            //Transfert Trames process
            while (network.isConnectionClose()) {
                try {
                    String trame = in.readLine();
                    trame = BitStuffing.unstuffing(trame);
                    network.verifyTrame(trame);

                    //DISPLAY transmission
                    Display.printL1("<--- Receiving \t");
                    if (trame == null)
                        Display.printL1("Incorrect Stuffing \t");
                    else
                        Display.showTrame(trame);
                    Display.printL3("\tWindowsSliding " + network.getStart() + " - " + network.getEnd());
                    Display.printL2("");

                } catch (SocketTimeoutException t) {
                    String trame = network.getNextTrame();
                    if (trame != null) {
                        //DISPLAY transmission
                        Display.printL1("\n\n---> Sending \t");
                        Display.showTrame(trame);
                        Display.printL3("\tWindowsSliding " + network.getStart() + " - " + network.getEnd());
                        trame = CorruptedServer.cTrame(trame);

                        trame = BitStuffing.stuffing(trame);

                        //DISPLAY transmission
                        trame = CorruptedServer.cStuffing(trame);
                        Display.printL2("");

                        if (!CorruptedServer.noSendingCorruption()) {
                            out.println(trame);
                            out.flush();
                        } else
                            Display.printL2("NEVER reach destination ... ");
                    }
                }
            }

            out.println(BitStuffing.stuffing(network.getNextTrame()));
            out.flush();

            Display.printL7("Closing Server");
            socket.close();
            in.close();
            return network.getData();
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
} 