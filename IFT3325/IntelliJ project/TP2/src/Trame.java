/**
 * Trame manipulation data
 */
public class Trame {
    /**
     * Sequence to build a trame
     * @param type  The type (Char)
     * @param number the number
     * @param binaryData The data
     * @return  Complete Trame
     */
    public static String buildTrame(String type, int number, String binaryData){
        String trame = "";
        trame += Converter.stringToBinary(type);
        trame += Converter.stringToBinary(Integer.toString(number));
        trame += binaryData;
        trame += CRC.calculCRC(trame);
        return trame;
    }

    /**
     * Get the number in trame
     * @param trame A trame
     * @return A number
     */
    public static int getNumber(String trame){
        return Converter.binaryToInt(trame.substring(8,16));
    }

    /**
     * Get the type of trame
     * @param trame A trame
     * @return A type
     */
    public static char getType(String trame) {
        return Converter.binaryToString(trame.substring(0,8)).charAt(0);
    }

    /**
     * Get the trame Data
     * @param trame A trame
     * @return The data
     */
    public static String getData(String trame){
        return trame.substring(16,trame.length()-16);
    }
}
