import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

    public static void main (String args[]) {
            System.out.println("Entrer une commande.");
            Scanner sc = new Scanner(System.in);
            String input = sc.nextLine();

            ArrayList<String> token = new ArrayList<>();

            //We consider Commande are good (no verifier)
            StringTokenizer st = new StringTokenizer(input, " ");
            while (st.hasMoreTokens()) {
                token.add(st.nextToken());
            }

            if (token.size() == 2) {
                    System.out.println("Faite un choix :\n 1 : Server sain \n 2: Server BitStuffing erreur Envoie \n 3 : Server Envoie perdu \n 4 : Server trame corrompu \n 5 : Server Completement corrompu");
                    input = sc.nextLine();
                    int n = Integer.parseInt(input);
                    switch (n) {
                        case 1 :
                            CorruptedServer.setCorruptedSending(100);
                            CorruptedServer.setCorruptedStuffing(100);
                            CorruptedServer.setCorruptedTrame(100);
                            break;
                        case 2 :
                            CorruptedServer.setCorruptedSending(100);
                            CorruptedServer.setCorruptedStuffing(2);
                            CorruptedServer.setCorruptedTrame(100);
                            break;
                        case 3:
                            CorruptedServer.setCorruptedSending(3);
                            CorruptedServer.setCorruptedStuffing(100);
                            CorruptedServer.setCorruptedTrame(100);
                            break;
                        case 4:
                            CorruptedServer.setCorruptedSending(100);
                            CorruptedServer.setCorruptedStuffing(100);
                            CorruptedServer.setCorruptedTrame(4);
                            break;
                        case 5 :
                            CorruptedServer.setCorruptedSending(3);
                            CorruptedServer.setCorruptedStuffing(2);
                            CorruptedServer.setCorruptedTrame(4);
                            break;
                        default :
                            System.out.println("Mauvais choix.");
                            return;
                    }

                    Server server = new Server();
                    String fileData = server.getBinFile(Integer.parseInt(token.get(1)));
                    if (fileData == null) {
                        Display.printL0("There is no Data");
                    }
                    else
                        AppFile.createTxtFromBin(fileData);

            } else if (token.size() == 5) {

                    String binData = AppFile.getFileToBinary(token.get(3));
                    if (binData.equals("")) {
                        Display.printL0("There is no file data");
                        return;
                    }

                    System.out.println("Faite un choix :\n 1 : Client sain \n 2: Client BitStuffing erreur Envoie \n 3 : Client Envoie perdu \n 4 : Client trame corrompu \n 5 : client completement Corrompu");
                    input = sc.nextLine();
                    int n = Integer.parseInt(input);
                    switch (n) {
                        case 1 :
                            CorruptedClient.setCorruptedSending(100);
                            CorruptedClient.setCorruptedStuffing(100);
                            CorruptedClient.setCorruptedTrame(100);
                            break;
                        case 2 :
                            CorruptedClient.setCorruptedSending(100);
                            CorruptedClient.setCorruptedStuffing(2);
                            CorruptedClient.setCorruptedTrame(100);
                            break;
                        case 3:
                            CorruptedClient.setCorruptedSending(3);
                            CorruptedClient.setCorruptedStuffing(100);
                            CorruptedClient.setCorruptedTrame(100);
                            break;
                        case 4:
                            CorruptedClient.setCorruptedSending(3);
                            CorruptedClient.setCorruptedStuffing(2);
                            CorruptedClient.setCorruptedTrame(4);
                            break;
                        case 5 :
                            break;
                        default :
                            System.out.println("Mauvais choix.");
                            return;
                    }

                    Client client = new Client();
                    client.start(token.get(1), Integer.parseInt(token.get(2)), binData, Integer.parseInt(token.get(4)));
            } else
                Display.printL0("Commande invalide");
        }
}
