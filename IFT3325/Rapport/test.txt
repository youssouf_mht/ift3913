Often in informal, non-technical language, 
concentration is described in a qualitative way, 
through the use of adjectives such as "dilute" for solutions 
of relatively low concentration and "concentrated" for solutions 
of relatively high concentration.