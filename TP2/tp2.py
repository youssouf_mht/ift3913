import numpy as np
import pandas as pd
import math

## probleme de path? reverifier lemplacement du fichier tp2_donness.csv
data = pd.read_csv("/home/mahamat/Downloads/Tp2_3913/tp2_donnees.csv")

# les arrays pour representer les differents champs
arr_nom = list(data["NOM"])
arr_nec = list(data["NEC"])
arr_dit = list(data["DIT"])
arr_cac = list(data["CAC"])


# Calcul des coefficients a et b pour trouver la droite de regression lineaire Y = aX + b
def regression(array1,array2):
    sxy = 0
    sxx = 0
    mx = np.mean(array1)
    my = np.mean(array2)
    for i in range(len(array1)):
        sxy += ((array1[i] - mx) * (array2[i] - my))
        sxx += ((array1[i] - mx)**2)
    b = sxy/sxx
    a = my - b*mx

    return (a, b)

# a) Le nombre d’erreurs est une fonction linéaire du NOM

print("1) Calcul de a et b pour NOM et NEC \n")
(a, b) = regression(arr_nom, arr_nec)
print( f'a = {a} , b = {b}')
print(" %.3fx +  %.3f" %(b,a))

# b) Le nombre d’erreurs est une fonction linéaire du DIT
print("2) Calcul de a et b pour DIT et NEC \n")
(a, b) = regression(arr_dit, arr_nec)
print( f'a = {a} , b = {b}')
print(" %.3fx +  %.3f" %(b,a))

print("3) Calcul de a et b pour CAC et NEC \n")
# c)Le nombre d’erreurs est une fonction linéaire du CAC

(a, b) = regression(arr_cac, arr_dit)
print( f'a = {a} , b = {b}')
print(" %.3fx +  %.3f" %(b,a))