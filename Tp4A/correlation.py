import pandas as pd

df = pd.read_csv('donnees.csv')

# utilisation de la methode spearman de pandas.

df.corr(method='spearman')

'''output de la ligne precedente.

          nb_class    m_c_BC
nb_class  1.000000 -0.547245
m_c_BC   -0.547245  1.000000


'''

m_c_BC = list(df['m_c_BC'])
nb_c = list(df['n_classes'])
import matplotlib.pyplot as plt

plt.xlabel('Nombre de classe',color='green') 
plt.ylabel('m_c_BC',color='red')

plt.scatter(nb_c,m_c_BC )

plt.show()
