import code.JavaMetricCalculator;

import java.io.File;
import java.io.IOException;

public class MetrixExecutor {


    synchronized public String exec(MetricThread metricThread){

        Runtime run = Runtime.getRuntime();
        try {
             String cmd = metricThread.getCMD() + metricThread.getId_version();
           //  System.err.println("Execution cmd " + cmd );

            Process p = run.exec(cmd,null,new File(metricThread.getDIR()));

            p.waitFor();
            String bc = new JavaMetricCalculator().get_mediane_and_class_len(metricThread.getDIR());
           // System.err.println("Execution Terminer cmd " + cmd );
            return bc;

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return "";


    }

    public MetrixExecutor(){};
}
