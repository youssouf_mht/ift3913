import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Versions extends Thread{

    private ArrayList<String> versions = new ArrayList<>();

    private ArrayList<String> reduce_version = new ArrayList<>();
    private String path;

    public ArrayList<String> getVersions() {
        return versions;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private static final String CMD = "git rev-list master " ;

    @Override
    public void run() {

        try {
            Runtime run = Runtime.getRuntime();

            // creer le processus et le fait runner
            Process p = run.exec(CMD,null,new File(path));

            // partie de la lecture de l'output de la commande.
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line ="";

            while((line=reader.readLine()) != null) {

                versions.add(line);

            }
            p.waitFor(); // attendre jusqu'a le processus fini.
            int newLent = versions.size();
            // si nous avons plus de 1000 versions
            // alors on choisir 10% pour tester
            // le choix se fait de facon aleatoire.
            if(versions.size() > 1000){
                double reduce = versions.size()/10.d;
                newLent = (int) Math.floor(reduce);
            }
            if(newLent != versions.size()){
                versions = getRandomElement(versions,newLent);
            }


        } catch (Exception e){

           // System.out.println("Erreur servenue " + e.getMessage() );
        }
    }

    public int getVersionsLength() {
        return versions.size();
    }
    public ArrayList<String> getRandomElement(ArrayList<String> list,
                                          int totalItems)
    {
        Random rand = new Random();

        // create a temporary list for storing
        // selected element
        ArrayList<String> newList = new ArrayList<>();
        for (int i = 0; i < totalItems; i++) {

            // take a raundom index between 0 to size
            // of given List
            int randomIndex = rand.nextInt(list.size());

            // add element in temporary list
            newList.add(list.get(randomIndex));

            // Remove selected element from orginal list
            list.remove(randomIndex);
        }
        return newList;
    }




}
