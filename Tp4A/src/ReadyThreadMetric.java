import java.util.ArrayList;

public class ReadyThreadMetric  extends Thread{

    private InitThread initThread;
    private Versions versions;

    private MetricThread[] metricThreads;


    public ReadyThreadMetric(InitThread init, Versions versions){
        this.versions = versions;
        this.initThread = init;
    }
    public InitThread getInitThread() {
        return initThread;
    }

    public void setInitThread(InitThread initThread) {
        this.initThread = initThread;
    }

    public Versions getVersions() {
        return versions;
    }

    public void setVersions(Versions versions) {
        this.versions = versions;
    }

    @Override
    public void run( ) {

        metricThreads = new MetricThread[versions.getVersionsLength()];
        ArrayList<String> ids = versions.getVersions();
        MetrixExecutor executor = new MetrixExecutor();
        MetricThread.setSize(ids.size());
        for (int j = 0; j < ids.size(); j++) {
            metricThreads[j] = new MetricThread();
            metricThreads[j].setId_version(ids.get(j));
            metricThreads[j].setIndex(j);
            metricThreads[j].setDIR(initThread.getPath());

        }
        for(int k=0; k< ids.size();k++){
            metricThreads[k].setMetrixExecutor(executor);
            //System.out.println(metricThreads[k]);
        }
    }

    public MetricThread[] getData() {
        return metricThreads;
    }
}
