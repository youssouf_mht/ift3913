


import com.opencsv.CSVWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {


	private static final String free_url = "https://github.com/jfree/jfreechart";

	private static final String git_rep = "https://github.com/nbouteme/projet-java";

	private static final String OUTPUT_FILE = "donnees.csv";

	public static void main(String[] args) throws IOException, InterruptedException {

		Scanner scanner = new Scanner(System.in);
		String type ="";
		System.out.println("Entre L'URL du git repository ou tapez d pour le url par defaut");
		type = scanner.nextLine();

		InitThread init = new InitThread();
		if(!type.equals("d")){
			init.setUrl(type);


		} else{

			init.setUrl(free_url);

		}
		init.start();
		init.join();

		System.out.println("Le repository a ete clone dans le repertoire " + init.getPath());

		 Versions vthread= new Versions();
		 vthread.setPath(init.getPath());
		 vthread.start();
		 vthread.join();
		 System.out.println("il ya " + vthread.getVersionsLength() +" versions.");

		ReadyThreadMetric readyThreadMetric =  new ReadyThreadMetric(init,vthread);
		readyThreadMetric.start();
		readyThreadMetric.join();

		System.out.println("tout est ready pour commencer le calcul...");

		MetricThread[] threads = readyThreadMetric.getData();
		ArrayList<String> versions = vthread.getVersions();
		// creation de thread et faire le run
		for(int j=0; j < vthread.getVersionsLength(); j++){

			threads[j].start();

		}
		// attendre que les thread finissent
		for(int j=0; j < versions.size(); j++){
			if(threads[j] != null)
			threads[j].join();
		}

		System.out.println("Tous les metriques sont calculer");
		 //tous les threads ont fini.


		BufferedWriter csvWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE));
		String head = "id_version,n_classes,m_c_BC\n";
		csvWriter.write(head);
		csvWriter.flush();
		for (MetricThread metricThread : threads){
			csvWriter.write(metricThread.toString());
			csvWriter.flush();
		}

		csvWriter.close();

		System.out.println("Le fichier des donnees CSV est cree");
		System.out.println("Les donnees sont dans le fichier " + OUTPUT_FILE);

		// delete the file
		deleteDir(new File(init.getPath()));
	}

	static void deleteDir(File file) {
		File[] contents = file.listFiles();
		if (contents != null) {
			for (File f : contents) {
				if (! Files.isSymbolicLink(f.toPath())) {
					deleteDir(f);
				}
			}
		}
		file.delete();
	}




}