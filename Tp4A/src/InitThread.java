import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class InitThread extends Thread{


    private String url;
    private static final String REPO = "GIT_REPO";
    private static final String CMD = "git clone ";

    private File file;

    public String getUrl() {
        return url;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private String path;

    public String getPath() {
        return (file == null)? new File("").getAbsolutePath()+"/"+REPO
                : file.getAbsolutePath();
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public void run() {
        try {

            String cmd = CMD + url + " " + new File("").getAbsolutePath()+"/"+ REPO;
            execute_command(cmd,new File("").getAbsolutePath());



        } catch (Exception e){

            System.out.println("Erreur dans init " + e.getMessage() );
        }

    }

    /**
     * Execute une commande shell dans un repertoire specifique
     * @param commdande la commande a executer
     * @param dir le repertoire ou la commande doit etre executer.
     * @return un ArrayList contenant les outputs de la commande.
     */
    public static void execute_command(String commdande, String dir){


        try {
            Runtime run = Runtime.getRuntime();
            // creer le processus et le fait runner
            Process p = run.exec(commdande,null,new File(dir));

            p.waitFor(); // attendre jusqu'a le processus fini.


        } catch (Exception e){

            System.out.println("Erreur servenue dans init " + e.getMessage() );
        }
    }
}
