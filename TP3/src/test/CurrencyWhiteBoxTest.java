package test;

import currencyConverter.Currency;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyWhiteBoxTest {
    //***************************************************
    //Couverture des instructions
    //***************************************************

    @Test
    void instruction1() {
        double convert = Currency.convert(20.55, 3.0);
        assertEquals(61.65, convert);
    }

    //***************************************************
    //Couverture des arcs du graphe de flot de contrôle
    //***************************************************
        //Nothing

    //***************************************************
    //Couverture des chemins indépendants du graphe de flot de contrôle
    //***************************************************
        //Nothing

    //***************************************************
    //Couverture des conditions
    //***************************************************
        //Round number test
    @Test
    void condition1(){
        double convert = Currency.convert(444.49, 0.01);
        assertEquals(4.44, convert, "Failed to round 4.4449 to 4.44");
    }

    @Test
    void condition2(){
        double convert = Currency.convert(444.50, 0.01);
        assertEquals(4.45, convert, "Failed to round 4.4450 to 44.45");
    }


    //***************************************************
    //Couverture des i-chemins
    //***************************************************
        //Nothing

}