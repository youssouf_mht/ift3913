package test;

import currencyConverter.Currency;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyBlackBoxTest {
    //Domaine Amount : [0, inf+]
        //Classe d'équiv :  <0, >=0
        //Jeu de test : -5.2, 4.2

    //Domaine exchangeValue : [0, inf+]
        //Classe d'équiv :  <0, >=0
        //Jeu de test : -3.1, 7.8

    //-1 est defini comme un retour d'erreur
    @Test
    void equiv1() {
        double convert = Currency.convert(-5.2, -3.1);
        assertEquals(-1, convert);
    }

    @Test
    void equiv2() {
        double convert = Currency.convert(-5.2, 7.8);
        assertEquals(-1, convert);
    }

    @Test
    void equiv3() {
        double convert = Currency.convert(4.2, -3.1);
        assertEquals(-1, convert);
    }

    @Test
    void equiv4() {
        double convert = Currency.convert(4.2, 7.8);
        assertEquals(32.76, convert);
    }


    //Domaine Amount : [0, inf+]
        //Classe d'équiv :  <0, >=0
        //Valeurs frontières : -Min(double), 0, Max(double)

    //Domaine exchangeValue : [0, inf+]
        //Classe d'équiv :  <0, >=0
        //Valeurs frontières : -Min(double), 0, Max(double)

    //-1 est defini comme un retour d'erreur
    @Test
    void frontiere1(){
        double convert = Currency.convert(-Double.MIN_VALUE, -Double.MIN_VALUE);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere2(){
        double convert = Currency.convert(0.0, -Double.MIN_VALUE);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere3(){
        double convert = Currency.convert(Double.MAX_VALUE, -Double.MIN_VALUE);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere4(){
        double convert = Currency.convert(-Double.MIN_VALUE, 0.0);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere5(){
        double convert = Currency.convert(0.0, 0.0);
        assertEquals(0, convert);
    }

    @Test
    void frontiere6(){
        double convert = Currency.convert(Double.MAX_VALUE, 0.0);
        assertEquals(0, convert);
    }

    @Test
    void frontiere7(){
        double convert = Currency.convert(-Double.MIN_VALUE, Double.MAX_VALUE);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere8(){
        double convert = Currency.convert(0.0, Double.MAX_VALUE);
        assertEquals(0, convert);
    }

    @Test
    void frontiere9(){
        double convert = Currency.convert(Double.MAX_VALUE, Double.MAX_VALUE);
        assertEquals(-1, convert);
    }
}