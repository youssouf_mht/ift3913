package test;

import currencyConverter.Currency;
import currencyConverter.MainWindow;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MainWindowWhiteBoxTest {
    ArrayList<Currency> currenciesFake = new ArrayList<>();

    //***************************************************
    //Couverture des instructions
    //***************************************************

    @Test
    void instruction1() {
        double convert = MainWindow.convert("US Dollar", "US Dollar", Currency.init(), 6.0);
        assertEquals(6.0, convert);
    }

    //***************************************************
    //Couverture des arcs du graphe de flot de contrôle
    //***************************************************

    //1-2-3-4-10-19
    @Test
    void flot1(){
        double convert = MainWindow.convert("US Dollar", "Euro", currenciesFake, 6.0);
        assertEquals(-1, convert);
    }

    //1-2-3-4-5-6-7-10-11-12-19
    @Test
    void flot2(){
        double convert = MainWindow.convert("X", "Euro", Currency.init(), 6.0);
        assertEquals(-1, convert);
    }

    //***************************************************
    //Couverture des chemins indépendants du graphe de flot de contrôle
    //***************************************************

    //Test 1
    @Test
    void independant1(){
        double convert = MainWindow.convert("Euro", "X", Currency.init(), 6.0);
        assertEquals(-1, convert);
    }

    //Test 2
    @Test
    void independant2(){
        double convert = MainWindow.convert("Euro", "Euro", Currency.init(), 6.0);
        assertEquals(6.0, convert);
    }

    //Test 3
    @Test
    void independant3(){
        double convert = MainWindow.convert("Y", "Euro", Currency.init(), 6.0);
        assertEquals(-1, convert);
    }


    //***************************************************
    //Couverture des conditions
    //***************************************************
        //Nothing

    //***************************************************
    //Couverture des i-chemins
    //***************************************************

    @Test
    void chemin1(){
        String[] currencieName = {"US Dollar", "Euro", "British Pound",  "Chinese Yuan Renminbi", "Japanese Yen"};
        double[] answer = {};

        for (int i=0 ; i < currencieName.length; i++) {
            double convert = MainWindow.convert(currencieName[i], currencieName[i], Currency.init(), 6.0);
            assertEquals(6.0, convert);
        }
    }
}