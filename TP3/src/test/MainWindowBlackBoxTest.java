package test;

import currencyConverter.Currency;
import currencyConverter.MainWindow;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MainWindowBlackBoxTest {
    //Domaine currency1 : US Dollar, Euro, British Pound, Swiss Franc, Chinese Yuan Renminbi, Japanese Yen
        //Classe d'equiv : D1 = domaine, D2 = String/D1
        //Jeu de test : Euro, X

    //Domaine currency2 : US Dollar, Euro, British Pound, Swiss Franc, Chinese Yuan Renminbi, Japanese Yen
        //Classe d'equiv : D1 = domaine, D2 = String/D1
        //Jeu de test : US Dollar, Y

    //Domaine currencies : currencies from public static ArrayList<Currency> init()
        //Classe d'equiv : D1 = domaine, D2 = currencies/D1
        //Jeu de test : currencies from public static ArrayList<Currency> init(), Empty list
    ArrayList<Currency> currenciesFake = new ArrayList<>();

    //Domaine amount : [0, inf+]
        //Classe d'equiv : <0, >=0
        //Jeu de test : -5, 6

    @Test
    void equiv1() {
        double convert = MainWindow.convert("Euro", "US Dollar", Currency.init(), 6.0);
        assertEquals(6.44, convert);
    }

    @Test
    void equiv2() {
        double convert = MainWindow.convert("Euro", "US Dollar", Currency.init(), -5.0);
        assertEquals(-1, convert);
    }

    @Test
    void equiv3() {
        double convert = MainWindow.convert("Euro", "US Dollar", currenciesFake, 6.0);
        assertEquals(-1, convert);
    }

    @Test
    void equiv4() {
        double convert = MainWindow.convert("Euro", "Y", Currency.init(), 6.0);
        assertEquals(-1, convert);
    }


    @Test
    void equiv5() {
        double convert = MainWindow.convert("X", "US Dollar", Currency.init(), 6.0);
        assertEquals(-1, convert);
    }


    //Domaine currency1 : US Dollar, Euro, British Pound, Swiss Franc, Chinese Yuan Renminbi, Japanese Yen
        //Classe d'equiv : D1 = domaine, D2 = String/D1
        //Valeur Frontiere : British Pound, British pound, ""

    //Domaine currency2 : US Dollar, Euro, British Pound, Swiss Franc, Chinese Yuan Renminbi, Japanese Yen
        //Classe d'equiv : D1 = domaine, D2 = String/D1
        //Valeur Frontiere : British Pound, British pound, ""

    //Domaine currencies : currencies from public static ArrayList<Currency> init()
        //Classe d'equiv : D1 = domaine, D2 = currencies/D1
        //Valeur Frontiere : Empty list


    //Domaine amount : [0, inf+]
        //Classe d'equiv : <0, >=0
        //Valeur Frontiere : -Min(double), 0, Max(double)

    @Test
    void frontiere1(){
        double convert = MainWindow.convert("British Pound", "British Pound", Currency.init(), 0.0);
        assertEquals(0.0, convert);
    }

    @Test
    void frontiere2(){
        double convert = MainWindow.convert("British pound", "British Pound", Currency.init(), 0.0);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere3(){
        double convert = MainWindow.convert("", "British Pound", Currency.init(), 0.0);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere4(){
        double convert = MainWindow.convert("British Pound", "British pound", Currency.init(), 0.0);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere5(){
        double convert = MainWindow.convert("British Pound", "", Currency.init(), 0.0);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere6(){
        double convert = MainWindow.convert("British Pound", "British Pound", currenciesFake, 0.0);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere7(){
        double convert = MainWindow.convert("British Pound", "British Pound", Currency.init(), -Double.MIN_VALUE);
        assertEquals(-1, convert);
    }

    @Test
    void frontiere8(){
        double convert = MainWindow.convert("British Pound", "British Pound", Currency.init(), Double.MAX_VALUE);
        assertEquals(-1, convert);
    }
}