package TestUnitaire;



import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import code.Class;
import org.junit.Assert;
import org.junit.Test;


import code.*;

import static org.junit.Assert.*;

public class MainTest {
	
	MetricParser parser = new MetricParser();
	ArrayList<code.Class> data = null;
	
	
	void init() {
		
		data = parser.calculMetricFromJavaFile("/Users/mac/Documents/UDEM/IFT3913A/jfreechart");
	}
    
	code.Class getClassByName(String name) {
		
		if(data == null) init();
		for(code.Class cl : data) {
            if (cl.getName().equals(name))
                return cl;
        }
		System.out.println("Erreur de nom ");
		return null;
	}
	
	@Test
	public void testClassXYPlot() {

        String path = "/Users/mac/Documents/UDEM/IFT3913A/jfreechart/src/main/java/org/jfree/chart/plot/XYPlot.java";

        int loc = 0;

        String line;
        try{
            BufferedReader reader = new BufferedReader( new FileReader(path));
            while ((line = reader.readLine()) != null){

                if(!line.trim().isBlank()){
                    loc++;
                }
            }

        } catch (Exception e){
            System.out.println("Erreur survenue");
        }

        MetricParser parser = new MetricParser();
        ArrayList<Class> tab = parser.calculMetricFromJavaFile(path);
        Class renderer = tab.get(tab.size()-1);

        assertEquals("LOC different",loc,renderer.getLoc());

	}



    @Test
    public void TestClassAbstractRenderer() throws IOException {



	    String path = "/Users/mac/Documents/UDEM/IFT3913A/ift3913/TP1/TestFolder/src/AbstractRenderer.java";

        int loc = 0;

        String line;
        try{
            BufferedReader reader = new BufferedReader( new FileReader(path));
            while ((line = reader.readLine()) != null){

                    if(!line.trim().isBlank()){
                        loc++;
                    }
            }

        } catch (Exception e){
            System.out.println("Erreur survenue");
        }

        MetricParser parser = new MetricParser();
        ArrayList<Class> tab = parser.calculMetricFromJavaFile(path);
        Class renderer = tab.get(tab.size()-1);
        assertEquals("LOC different",loc,renderer.getLoc());

    }






}
