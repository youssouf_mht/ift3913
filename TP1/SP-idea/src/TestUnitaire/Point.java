package TestUnitaire;

public class Point{


	private int x; // abcisse de l'ordonne x
	private  int y; // abcisse de l'ordonne y
	// le constructeur
	public Point(int x,int y){
		this.x = x;
		this.y = y;
	}
	
	
	public int getX(){ return x;}
	
	public int getY(){return y;}
	
	public void setX(int x){ this.x = x; }
	
	public void setY(int y){ this.y = y; }
	
}
	