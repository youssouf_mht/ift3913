package code;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import com.opencsv.CSVWriter;

public class CSV {


    /**
     * Cette methode genere les fichiers CSV des classes et methodes
     * @param javaMap le HashMap de tous les classes du fichier
     */
    public static void createCSV(HashMap<String, HashMap<String, Class>> javaMap) {
        ArrayList<String[]> methods = new ArrayList<String[]>();
        ArrayList<String[]> classes = new ArrayList<String[]>();

        String[] methodsHeader = {"chemin", "class", "methode", "methode_LOC", "methode_CLOC", "methode_DC", "CC", "methode_BC"};
        methods.add(methodsHeader);

        String[] classHeader = {"chemin", "class", "classe_LOC", "classe_CLOC", "classe_DC", "WMC", "classe_BC"};
        classes.add(classHeader);

        String loc, cloc, dc, cc, bc, wmc;

        int nbMethods = 0, nbClasses = 0;

        //Premiere map : les path (outtermap)
        for (String absolutePath : javaMap.keySet())
            //2e map : les classes (innermap)
            for (Class c : javaMap.get(absolutePath).values()){
                //Map des methods dans une classe
                for (Method m: c.getMethods().values()){
                    nbMethods++;
                    loc = Integer.toString(m.getLoc());
                    cloc = Integer.toString(m.getCloc());
                    dc = String.format("%.10f", m.getDc());
                    cc = Integer.toString(m.getCc());
                    bc = String.format("%.10f", m.getBc());

                    String[] methodLine = {absolutePath, c.getName(), m.getName(), loc, cloc, dc, cc, bc};
                    methods.add(methodLine);
                }

                nbClasses++;
                loc = Integer.toString(c.getLoc());
                cloc = Integer.toString(c.getCloc());
                dc = String.format("%.10f", c.getDc());
                wmc = Integer.toString(c.getWmc());
                bc = String.format("%.10f", c.getBc());

                String[] classLine = {absolutePath, c.getName(), loc, cloc, dc, wmc, bc};
                classes.add(classLine);
            }

        //Creation des CSV
        System.out.println("Il y a " + nbMethods + " methodes et " + nbClasses + " classes detectes.");
        createMethodCSV(methods);
            createClassCSV(classes);
    }


    private static void createMethodCSV( ArrayList<String[]> methods){
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter("methodes.csv"));

            for (int i=0; i < methods.size(); i++)
                csvWriter.writeNext(methods.get(i));

            csvWriter.close();
            System.out.println("Le fichier methodes.csv  est cree.");

        } catch (Exception e) {
            System.out.println("Une erreur est survenue. \n" + e.getMessage());
        }
    }

    private static void createClassCSV(ArrayList<String[]> classes){
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter("classes.csv"));

            for (int i=0; i < classes.size(); i++)
                csvWriter.writeNext(classes.get(i));

            csvWriter.close();
            System.out.println("Le fichier classes.csv  est cree.");

        } catch (Exception e) {
            System.out.println("Une erreur est survenue. \n" + e.getMessage());
        }

    }
}
