package code;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Directory {

    /**
     * Methode demandant a l'utilisateur le chemin d'un dossier et ensuite vefifie la validite de l'entree.
     * @return le chemin du dossier (User Input).
     */
    public static String askUserDirectoryLocation () {
        Scanner userInput = new Scanner(System.in);
        boolean isPathExist = false, isDirectory = false;
        String dir = "";

        do {
            try {
                System.out.print("Entrer le chemin d'un dossier : ");
                dir = userInput.nextLine();

                Path path = Paths.get(dir);
                isPathExist = Files.exists(path);
                isDirectory = Files.isDirectory(path);

                if (!isPathExist) System.out.println("Le chemin n'existe pas. Essayer a nouveau.");
                if (!isDirectory) System.out.println("Ce n'est pas un dossier. Essayer a nouveau.");

            } catch (Exception e) {
                System.out.println("Une erreur est survenue. \n" + e.getMessage() + "\n");
            }
        } while (!isPathExist || !isDirectory);

        return dir;
    }

    /**
     * Trouve toutes les fichiers .java d'un dossier et des ses sous-dossiers.
     * @param sourceDirectory Le dossier source
     * @return retourne sous forme de Map<path, fileName> tous les fichiers .java toruvés
     */
    public static Map<String, String> getAllJavaFiles (String sourceDirectory) {
        //Map<absolutePath, fileName>
        HashMap<String, String> javaFiles = new HashMap<String, String>();
        File mainFile = new File(sourceDirectory);
        javaFiles = recursiveJavaFilesFinder(mainFile, javaFiles);
        return javaFiles;
    }

    private static HashMap<String, String> recursiveJavaFilesFinder (File dir, HashMap<String, String> javaFiles){
        File[] files = dir.listFiles();
        for (File file : files){
            //Dossier?
            if (file.isDirectory()) {
                recursiveJavaFilesFinder(file, javaFiles);
            } else {
                //Verification du nom du fichier
                String fileName = file.getName();
                fileName = fileName.toLowerCase();
                if (fileName.endsWith(".java")){
                    javaFiles.put(file.getAbsolutePath(), file.getName());
                }
            }
        }
        return javaFiles;
    }



}
