package code;
public class Method {
    private String name; // le nom de la methode
    private int loc;
    private int cloc;
    private int cc;

    /**
     * Le constructeur pour initialiser les attributs de la classe
     */
    public Method(String name, int loc, int cloc, int cc) {
        this.name = name;
        this.loc = loc;
        this.cloc = cloc;
        this.cc = cc;
    }

    /**
     *
     * @return retourne le nom de la classe
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return retourne le LOC de la methode
     */
    public int getLoc() {
        return loc;
    }

    /**
     *
     * @return retourne Le CLOC de la methode
     */
    public int getCloc() {
        return cloc;
    }

    /**
     *
     * @return retourne le Dc de la methode courante
     */
    public double getDc() {
        return (loc > 0) ?  (double) cloc/loc : 0;
    }
    /**
     * retourne le Cc de la methode
     * @return
     */
    public int getCc() {


        return cc;
    }

    /**
     * retourne le Bc de la methode
     * @return
     */
    public double getBc() {
       int val = getCc();
       return (val > 0 ) ?  getDc()/val : 0;
    }

    /**
     * ajoute au LOC la valeur x
     * @param x un entier
     */
    public void addLOC(int x){
        this.loc += x;
    }

    /**
     * AJOUTE AU CLOC x
     * @param x un entier
     */
    public void addCLOC(int x){
        this.cloc += x;
    }
}
