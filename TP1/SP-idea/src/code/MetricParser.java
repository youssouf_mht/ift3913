package code;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MetricParser {
    //Liste des classes lues
    private ArrayList<Class> classList = new ArrayList<Class>();

    //Nombre de ligne de la JavaDoc sur une Classe ou Methode
    private ArrayList<Integer> commentHeader = new ArrayList<>();

    //Liste des méthodes des classes
    private ArrayList<HashMap<String, Method>> methodsList = new ArrayList<>();

    /**
     * Storage des metriques calcules
     */
    private class MetricType {
        public int col = 0;
        public int cloc = 0;
        public int cc = 0;
        public int parenthesisLevel = 0;
    }

    /**
     * Retourne la liste des classe du fichier pointer.
     * @param path le chemin d'un fichier
     * @return Retourne les classes d'un fichier .java
     */
    public ArrayList<Class> calculMetricFromJavaFile(String path){
        Scanner openedFile = openFile(path);
        if (openedFile == null)
            return classList;

        mainFileParser(openedFile);
        return classList;
    }

    //Ouvre un fichier .java
    private Scanner openFile(String filePath){
        Scanner openedFile = null;
        try {
            File file = new File(filePath);
            openedFile = new Scanner(file);
        } catch (Exception e){
            System.out.println("Une erreur est survenue. \n" + e.getMessage());
        }
        return openedFile;
    }

    //Parsing du fichier principal
    private void mainFileParser(Scanner javaFileOpened) {
        int col = 0;
        int cloc = 0;
        commentHeader.add(0);

        while (javaFileOpened.hasNextLine()){
            MetricType metric = regexFinder(javaFileOpened);
            col += metric.col;
            cloc += metric.cloc;
        }
        javaFileOpened.close();

        //Fichier .java sans classe?
        if (this.classList.isEmpty())
                return;

        //La derniere classe est la classe principale
        Class c = this.classList.get(this.classList.size() - 1);

        //Les metriques ont deja calculer ceux de la classe principale
        col -= c.getLoc();
        cloc -= c.getCloc();

        //Ajout des metriques manquants à la classe principale (Header et Footer)
        c.addLOC(col);
        c.addCLOC(cloc);
    }

    private static final Pattern regexSingleLineComment = Pattern.compile("//(.*)");
    private static final Pattern regexMultiLineComment = Pattern.compile("/\\*.*");
    private static final Pattern regexMultiLineCommentEnd = Pattern.compile("\\*/");
    private static final Pattern regexClass = Pattern.compile("(\\s(class|interface|enum)|(^class|^interface|^enum))\\s+.*\\{");
    private static final Pattern regexClassName = Pattern.compile("(class|interface|enum)\\s+\\w+");
    private static final Pattern regexMethod = Pattern.compile("\\w+\\s+(\\w+\\s*)+\\(.*\\).*\\{");
    private static final Pattern regexMethodName = Pattern.compile("\\s\\w+\\s*\\(.*\\)");
    private static final Pattern regexCC = Pattern.compile("if\\s*\\(.*\\)|case\\s*.*\\s*:|for\\s*\\(.*\\)|while\\s*\\(.*\\)");
    private static final Pattern regexString = Pattern.compile("\"(?:(?=(\\\\?))\\1.)*?\"");
    private static final Pattern regexParenthesisOpen = Pattern.compile("\\{");
    private static final Pattern regexParenthesisClose = Pattern.compile("\\}");
    private static final Pattern regexEndLine = Pattern.compile("[;\\}//{]");

    //Analyse d'une ligne de code
    private MetricType regexFinder(Scanner javaFileOpened){
        MetricType metric = new MetricType();
        String data = "";

        Matcher mEndLine;

        //Boucle pour ramifier les lignes de code écrite sur plus d'une ligne
        do {
            //Read NextLine
            data += javaFileOpened.nextLine();

            //Ligne vide?
            if (data.trim().isBlank())
                return metric;

            //Ligne de code
            metric.col += 1;

            //Remove anything between ""
            Matcher mString = regexString.matcher(data);
            data = mString.replaceAll("");

            //Multi Line Comments
            Matcher mMutliCommentLine = regexMultiLineComment.matcher(data);
            if (mMutliCommentLine.find()) {
                boolean isEnd = false;

                while (!isEnd) {
                    Matcher mCommentEnd = regexMultiLineCommentEnd.matcher(data);
                    metric.cloc += 1;

                    //Fin du bloc de commentaire?
                    if (mCommentEnd.find()) {
                        isEnd = true;

                        //Delete Multi Line comment
                        Pattern regexMultiLineCommentComplete = Pattern.compile("/\\*.*\\*/");
                        mMutliCommentLine = regexMultiLineCommentComplete.matcher(data);
                        data = mMutliCommentLine.replaceAll("");
                    } else {
                        data += javaFileOpened.nextLine();
                        metric.col += 1;
                    }
                }
                //Sauvegarde du bloc de commentaire (il peut s'agir d'une javaDoc d'une classe ou Methode ultérieur)
                this.commentHeader.set(this.commentHeader.size() - 1, metric.cloc + this.commentHeader.get(this.commentHeader.size() - 1));
            }


            //Single comment (w or w/ code before)
            Matcher mSingleComment = regexSingleLineComment.matcher(data);
            if (mSingleComment.find()) {
                metric.cloc += 1;
                data = mSingleComment.replaceAll("");

                //Sauvegarde du bloc de commentaire (il peut s'agir d'un commentaire au-dessus d'une classe ou Methode ultérieur)
                this.commentHeader.set(this.commentHeader.size() - 1, metric.cloc + this.commentHeader.get(this.commentHeader.size() - 1));
            }

            //c'etais que des commentaires?
            if (data.trim().isBlank())
                return metric;

            //Class detection
            Matcher mClass = regexClass.matcher(data);
            if (mClass.find())
                return classParser(javaFileOpened, data, metric);

            //CC
            Matcher mCC = regexCC.matcher(data);
            if (mCC.find())
                metric.cc += 1;
            else {
                //Method Detected
                Matcher mMethod = regexMethod.matcher(data);
                if (mMethod.find())
                    return methodParser(javaFileOpened, data, metric);
            }

            //Bracket {
            Matcher mOpenParenthesis = regexParenthesisOpen.matcher(data);
            while (mOpenParenthesis.find())
                metric.parenthesisLevel += 1;

            //Bracket }
            Matcher mCloseParenthesis = regexParenthesisClose.matcher(data);
            while (mCloseParenthesis.find())
                metric.parenthesisLevel -= 1;

            //Detecte les fins de ligne normal tel que {  et ;
            mEndLine = regexEndLine.matcher(data);
        }while(!mEndLine.find());

        //Reset le bloc de commentaire a 0 (i.e. ligne de code = pas un commentaire au-dessus d'une classe ou methode)
        this.commentHeader.set(this.commentHeader.size()-1, 0);
        return metric;
    }

    //Classe Partern detected
    private MetricType classParser(Scanner javaFileOpened, String data, MetricType metricHead){
        String name = parsingClassName(data);
        MetricType metricClass = new MetricType();
        metricClass.parenthesisLevel += 1;

        //Sauvegarde le nombre de ligne de l'entete de la classe
        metricClass.col += metricHead.col;

        //Ajout d'une profondeur au variable globale
        this.commentHeader.add(0);
        this.methodsList.add(new HashMap<String, Method>());

        //Si la classe est courte et termine sur la même ligne (i.e.  Enum A {a,c})
        Matcher mCloseParenthesis = regexParenthesisClose.matcher(data);
        if(mCloseParenthesis.find())
            metricClass.parenthesisLevel -= 1;

        //Analyse de la classe jusqu'a sa parenthese fermante
        while (metricClass.parenthesisLevel != 0) {
            MetricType metric = regexFinder(javaFileOpened);
            addMetricBtoA(metricClass, metric);
        }

        //Enleve une profondeur a la variable global
        this.commentHeader.remove(this.commentHeader.size()-1);

        //Ajout des commentaires lues avant la classe
        metricClass.cloc += this.commentHeader.get(this.commentHeader.size()-1);
        metricClass.col += this.commentHeader.get(this.commentHeader.size()-1);

        Class c = new Class (name, metricClass.col, metricClass.cloc, this.methodsList.get(this.methodsList.size()-1));

        //Enleve une profondeur a la variable globale
        this.methodsList.remove(this.methodsList.size()-1);

        //Ajout de la classe à la variable globale de sortie
        this.classList.add(c);

        //Enleve les commentaires lues avant la classe aux metriques de retour car la classe parents les as deja calculer
        metricClass.cloc -= this.commentHeader.get(this.commentHeader.size()-1);
        metricClass.col -= this.commentHeader.get(this.commentHeader.size()-1);

        return metricClass;
    }

    //Method Pattern detecte
    private MetricType methodParser(Scanner javaFileOpened, String data, MetricType metricHead){
        String name = parsingMethodName(data);
        MetricType metricMethod = new MetricType();
        metricMethod.parenthesisLevel += 1;

        //Sauvegarde le nombre de ligne de l'entete de la methode
        metricMethod.col += metricHead.col;

        //Ajout d'une profondeur a la variable globale
        this.commentHeader.add(0);

        //Si la methode est courte et termine sur la même ligne (e.i. void A {a,c})
        Matcher mCloseParenthesis = regexParenthesisClose.matcher(data);
        if(mCloseParenthesis.find())
            metricMethod.parenthesisLevel -= 1;

        //Analyse de la methode jusqu'a sa parenthese fermante
        while (metricMethod.parenthesisLevel != 0){
            MetricType metric = regexFinder(javaFileOpened);
            addMetricBtoA(metricMethod, metric);
        }

        //Enleve une profondeur a la variable global
        this.commentHeader.remove(this.commentHeader.size()-1);

        //Ajout des commentaires lues avant la methodes
        metricMethod.cloc += this.commentHeader.get(this.commentHeader.size()-1);
        metricMethod.col += this.commentHeader.get(this.commentHeader.size()-1);
        metricMethod.cc += 1;

        Method m = new Method(name, metricMethod.col, metricMethod.cloc, metricMethod.cc);

        //Ajout de la methodes a la liste des methodes de la classe courante
        this.methodsList.get(this.methodsList.size()-1).put(m.getName(), m);

        //Enleve les commentaires lues avant la methodes aux metriques de retour car la classe parent les as deja calculer
        metricMethod.cloc -= this.commentHeader.get(this.commentHeader.size()-1);
        metricMethod.col -= this.commentHeader.get(this.commentHeader.size()-1);

        return metricMethod;
    }

    //Retourne le Nom d'une methode
    private String parsingMethodName(String data){
        String methodName = "";

        //Extraire le nom et argument ()
        Matcher mMethodName = regexMethodName.matcher(data);
        mMethodName.find();
        String firstTrim = data.substring(mMethodName.start(),mMethodName.end());


        //Extraire le nom seulement
        Pattern regexName = Pattern.compile("\\w+");
        Matcher mName = regexName.matcher(firstTrim);
        mName.find();
        methodName += firstTrim.substring(mName.start(), mName.end());

        //Prendre ce qu'il y a entre () en les excluant
        String secondTrim = firstTrim.substring(mName.end()+1, firstTrim.length()-1);

        //Enlever text entre [] et <>
        Pattern regexSingleQuote = Pattern.compile("<(?:(?=(\\\\?))\\1.)*?>");
        Matcher mSingleQuote = regexSingleQuote.matcher(secondTrim);
        secondTrim = mSingleQuote.replaceAll("<>");
        Pattern regexArray = Pattern.compile("\\[(?:(?=(\\\\?))\\1.)*?\\]");
        Matcher mArray = regexArray.matcher(secondTrim);
        secondTrim = mArray.replaceAll("[]");

        //Les arguments sont les mots en position impair dans le string
        Scanner scan = new Scanner(secondTrim);
        int i = 0;
        while (scan.hasNext()){
            String arg = scan.next();
            i++;
            if(i%2 == 1)
                methodName += "_" + arg;
        }

        return methodName;
    }

    //Retourne le Nom d'une classe
    private String parsingClassName(String data){

        //Extraire le mot suivant class
        Matcher mClassName = regexClassName.matcher(data);
        mClassName.find();
        String name = data.substring(mClassName.start(),mClassName.end());

        //Le nom est toujours le mot après "class"
        Scanner scan = new Scanner(name);
        scan.next();
        name = scan.next();

        return name;
    }

    //Ajout B a A pour les metrics
    private void addMetricBtoA(MetricType a, MetricType b){
        a.cloc += b.cloc;
        a.col += b.col;
        a.parenthesisLevel += b.parenthesisLevel;
        a.cc += b.cc;
    }
}
