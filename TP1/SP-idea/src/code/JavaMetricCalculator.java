//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package code;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JavaMetricCalculator {
    private HashMap<String, HashMap<String, Class>> javaFiles = new HashMap();

    public JavaMetricCalculator() {
    }

    private void analyzePaths(Map<String, String> pathAndFiles) {
        Iterator var2 = pathAndFiles.keySet().iterator();

        while(true) {
            String path;
            ArrayList classeList;
            do {
                if (!var2.hasNext()) {
                    return;
                }

                path = (String)var2.next();
                MetricParser parser = new MetricParser();
                classeList = parser.calculMetricFromJavaFile(path);
            } while(classeList.isEmpty());

            HashMap<String, Class> classMap = new HashMap();

            for(int i = 0; i < classeList.size(); ++i) {
                classMap.put(((Class)classeList.get(i)).getName(), (Class)classeList.get(i));
            }

            this.javaFiles.put(path, classMap);
        }
    }

    public ArrayList<Double> getBcClass(String dir) {
        ArrayList<Double> class_bc = new ArrayList();
        ArrayList<String> test = new ArrayList();
        Map<String, String> javaFilesAndPaths = Directory.getAllJavaFiles(dir);
        this.analyzePaths(javaFilesAndPaths);
        if (!this.javaFiles.isEmpty()) {
            Iterator var5 = this.javaFiles.keySet().iterator();

            while(var5.hasNext()) {
                String absolutePath = (String)var5.next();
                Iterator var7 = ((HashMap)this.javaFiles.get(absolutePath)).values().iterator();

                while(var7.hasNext()) {
                    Class c = (Class)var7.next();
                    double bc = c.getBc();
                    test.add(absolutePath);
                    class_bc.add(bc);
                }
            }
        }

        return class_bc;
    }

    public void start() {
        System.out.println("Analyseur metric d'un dossier contenant des fichiers Java (.java).");
        String dir = Directory.askUserDirectoryLocation();
        Map<String, String> javaFilesAndPaths = Directory.getAllJavaFiles(dir);
        System.out.println("Il y a " + javaFilesAndPaths.size() + " fichier .java a analyser.");
        this.analyzePaths(javaFilesAndPaths);
        if (this.javaFiles.isEmpty()) {
            System.out.println("Erreur : Aucune classe java detecte.");
        } else {
            CSV.createCSV(this.javaFiles);
        }
    }

    public int getClassLength(String dir) {
        Map<String, String> javaFilesAndPaths = Directory.getAllJavaFiles(dir);
        this.analyzePaths(javaFilesAndPaths);
        if (this.javaFiles.isEmpty()) {
            System.out.println("Erreur : Aucune classe java detecte.");
            return 0;
        } else {
            return this.javaFiles.size();
        }
    }

    public static double get_mediane(ArrayList<Double> class_bc) {
        if (class_bc.size() == 0) {
            return 0.0D;
        } else {
            class_bc.sort(Comparator.naturalOrder());
            double mediane = 0.0D;
            int n = class_bc.size();
            if (class_bc.size() % 2 == 1) {
                mediane = (Double)class_bc.get(n / 2);
            } else {
                double median1 = (Double)class_bc.get(n / 2);
                double median2 = (Double)class_bc.get(n / 2 - 1);
                mediane = (median1 + median2) / 2.0D;
            }

            return mediane;
        }
    }

    public String get_mediane_and_class_len(String dir) {
        ArrayList<Double> bc = this.getBcClass(dir);
        String res = "";
        double mediane = get_mediane(bc);
        String m  = String.format("%.10f", mediane);
        res = bc.size()+","+ m;
        return res;
    }
}
