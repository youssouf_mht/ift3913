package code;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {


    /**
     * Le main qui roule le programme
     * @param args
     */
    public static void main(String[] args) {
        JavaMetricCalculator analyze = new JavaMetricCalculator();

        System.out.println("Appuyer sur ENTER pour quitter.");
        Scanner userInput = new Scanner(System.in);
        String dir = userInput.nextLine();

        String bc = analyze.get_mediane_and_class_len(dir);
        System.out.println(bc);
    }
}
