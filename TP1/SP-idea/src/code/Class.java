package code;
import java.util.HashMap;
import java.util.Map;

public class Class {
    private String name;
    private int loc;
    private int cloc;

    private HashMap<String, Method> methods;


    /**
    Le constructeur de la classe
    */
    public Class(String name, int loc, int cloc, HashMap<String, Method> methods) {
        this.name = name;
        this.loc = loc;
        this.cloc = cloc;
        this.methods = methods;
    }

    /**
    retourne le nom de la classe
    */
    public String getName() {
        return name;
    }

    /**
    retourne le LOC de la classe
    courante
    */
    public int getLoc() {
        return loc;
    }

    /**
    retourne le CLOC de la classe
    courante
    */
    public int getCloc() {
        return cloc;
    }


    /**
    retourne le Dc de la classe
    courante
    */
    public double getDc() {
        return (loc > 0) ?  (double) cloc/loc : 0;
    }



    /**
    retourne la Map des methodes de la classe
    */
    public Map<String, Method> getMethods() {
        return methods;
    }


    /**
    retourne le Wmc de la classe
    courante
    */
    public int getWmc() {

        int wmc = 0;
        for (Method m : methods.values())
            wmc += m.getCc();
        return wmc;
    }


    /**
    retourne le Bc de la classe
    courante
    */
    public double getBc() {

        int val = getWmc();
        return (val > 0 ) ? getDc()/ val: 0;
    }


    /**
    ajoute le nombre de   LOC de la classe
    courante
    */
    public void addLOC(int x){
        this.loc += x;
    }

    /**
    ajoute le nombre de   CLOC de la classe
    courante
    */
    public void addCLOC(int x){
        this.cloc += x;
    }
    
    public String toString() {
    	return "NameClasse " +name +" LOC= "+loc+", CLOC= " +cloc+", Dc= " + getDc();
    }
}
