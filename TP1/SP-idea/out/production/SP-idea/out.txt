import java.util.ArrayList;



public class RepertoireClient implements IRepertoire {


    private static ArrayList<Membre> map = new ArrayList<>();
    private static RepertoireClient single;
    
    public static RepertoireClient getInstance() {
	return (single == null)? new RepertoireClient(): single;
    }
	public void add_member(Membre p){
	    map.add(p)
;	}

	public void update_member(int code,String phone, String email,String address){
	   
	    Membre member = findById(code);
	    if(member == null) {
		System.out.println("Membre inexistant ");
		return;
	    }
	    
	    if(phone.length() > 0 ) {
		member.setPhone(phone);
	    }
	    if(email.length() > 0) {
		member.setEmail(email);
	    }
	    
	    if(address.length() > 0) {
		member.setAddresse(address);
	    }
	}

	/**
	     * procedure pour suprrimer un membre dans le repertoire de membre
	     * @param id
	     * @return true si supprimer sinon false
	*/
	public boolean delete(int id) {
	     int j = -1;

	     for (int i = 0; i < map.size(); i++) {
		 if (map.get(i).getUid() == id) {
	               j = i;
	                break;
		 }
	     }

	        if (j >= 0) {
	            map.remove(j);
	        }
	        return j >= 0;
	}



	public IRepertoire create_new(){
	    return null;
	}

	public void modify_data(){}

	public boolean delete_one(){
	    
	    return false;
	}
	
	public int generate_id() {
	    return generateUid();
	}
	
	
	public static int generateUid() {
	    
	    boolean uniq  = true;
	    int rand;
	    
	    do {
		rand = (int) (Math.floor(Math.random() * 1000000000 )+ 45);
		for (Humain p : map) {
		    if (p.getUid() == rand) {
			uniq = false;
			break;
		    }
		}
	    } while ( !uniq);
	  
	    return rand;
	}
	    
	    /**
	     * Cible un membre en particulier
	     * @param id
	     * @return le membre s'il existe sinon null
	     */
	    public Membre findById(int id) {
	        Membre result = null;
	        for (int i = 0; i < map.size(); i++) {
	            if (map.get(i).getUid() == id) {
	                result = map.get(i);
	                break;
	            }
	        }
	        return result;
	    }
	

}